# OpenML dataset: Kaggle_bike_sharing_demand_challange

https://www.openml.org/d/1414

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Niels Rood  
**Source**: [original](http://www.kaggle.com/c/bike-sharing-demand/data) - 2015-02-11  
**Please cite**:   

Modified version of the training dataset of the Bike Sharing Demand challenge running on Kaggle (http://www.kaggle.com/c/bike-sharing-demand/)

If you use the problem in publication, please cite:

Fanaee-T, Hadi, and Gama, Joao, Event labeling combining ensemble detectors and background knowledge, Progress in Artificial Intelligence (2013): pp. 1-15, Springer Berlin Heidelberg.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1414) of an [OpenML dataset](https://www.openml.org/d/1414). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1414/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1414/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1414/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

